require_dependency "content_engine/application_controller"

module ContentEngine
  class BlogsController < ApplicationController
    before_action :set_blog, only: [:show, :edit, :update, :destroy]
    
    # GET /blogs
    def index
      @blogs = Blog.all
    end

    # GET /blogs/1
    def show
    end

    # GET /blogs/new
    def new
      @blog = Blog.new
    end

    # GET /blogs/1/edit
    def edit
    end

    # POST /blogs
    def create
      @blog = Blog.new(blog_params)
      @blog.user_id = @current_user.id 
      if @blog.save
        redirect_to @blog, notice: 'Blog successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /blogs/1
    def update
      if @blog.update(blog_params)
        redirect_to @blog, notice: 'Blog successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /blogs/1
    def destroy
      @blog.destroy
      redirect_to blogs_url, notice: 'Blog successfully deleted.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_blog
        @blog = Blog.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def blog_params
        params.require(:blog).permit(:name, :description, :user_id, comments_attributes: [:id, :content, :user_id, :blog_id, :'_destroy'])
      end
  end
end
