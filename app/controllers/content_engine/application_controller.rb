module ContentEngine
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_action :current_user

    private

    def current_user
    	if session[:user_id].present?    		
    		@current_user = AuthenticationEngine::User.find_by(id: session[:user_id])
    	else
    		@current_user = nil
            redirect_to '/'
    	end 
    end

  end
end
