require_dependency "content_engine/application_controller"

module ContentEngine
  class WelcomeController < ApplicationController
    def index
    end
  end
end
