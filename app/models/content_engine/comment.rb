module ContentEngine
  class Comment < ApplicationRecord
  	belongs_to :blog
    belongs_to :user, class_name: AuthenticationEngine::User
  end
end
