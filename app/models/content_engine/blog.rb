module ContentEngine
  class Blog < ApplicationRecord
  	has_many :comments
  	accepts_nested_attributes_for :comments
  	belongs_to :user, class_name: AuthenticationEngine::User
  	## Validations
  	validates :name, :description, presence: true
  end
end
