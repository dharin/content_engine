# This migration comes from contact_engine (originally 20170321105213)
class CreateContactEngineComments < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_engine_comments do |t|
      t.integer :blog_id
      t.integer :user_id
      t.text :content

      t.timestamps
    end
  end
end
