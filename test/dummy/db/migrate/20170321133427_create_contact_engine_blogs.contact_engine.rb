# This migration comes from contact_engine (originally 20170321105118)
class CreateContactEngineBlogs < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_engine_blogs do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
