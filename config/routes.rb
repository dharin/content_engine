ContentEngine::Engine.routes.draw do
	resources :blogs
	mount AuthenticationEngine::Engine => "/"
end
