$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "content_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "content_engine"
  s.version     = ContentEngine::VERSION
  s.authors     = ["dharin"]
  s.email       = ["dharin.rajgor@gmail.com"]
  #s.homepage    = "TODO"
  s.summary     = "Summary of ContentEngine."
  s.description = "Description of ContentEngine."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.2"
  s.add_dependency "nested_form"
  s.add_development_dependency "sqlite3"
end
